<style><?php include 'includes/main.css'; ?> </style>

<!DOCTYPE html>
<html>
  <head>
    <title>test</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  </head>
  <body>
    <!-- Header section starts here -->
    <div class="block">
    <form action="includes/addpage.php" method="POST">
      <div class="grid-container">
        <div class="grid-item">
          <h2>Product Add</h2>
        </div>
      <div class="grid-item">
        <div class="grid-button">
          <button type="submit" name="submit">Save</button>
        </div>
      </div>
      </div>
      <hr>
    </div>
    <!-- End of header -->

    <!-- Product table starts here -->
    <table>
      <tr>
        <td>SKU</td>
        <td><input type="text" name="sku"></td>
      </tr>
      <tr>
        <td>Product Name</td>
        <td><input type="text" name="name"></td>
      </tr>
      <tr>
        <td>Price</td>
        <td><input type="text" name="price"></td>
      </tr>
    <!-- beginning of dropdown -->
      <tr>
        <td>Type</td>
        <td>
          <select id="types" name="type" onchange="onTypeSelected()">
            <option value=""></option>
            <option value="DVD">DVD</option>
            <option value="Book">Book</option>
            <option value="Furniture">Furniture</option>
          </select>
        </td>
      </tr>
    <!-- End of dropdown -->
    </table>
    <!-- End of table -->

    <!-- New table for the products listed in the dropdown menu -->
    <table >
      <br>
      <tr id="DVD">
        <td class="space">Size</td>
        <td class="td1">
          <input for="DVD" type="text" name="size">
        </td>
        <td class="td">Please provide size in GB format.</td>
      </tr>

      <tr id="Book">
        <td class="space">Weight</td>
        <td class="td1">
          <input for="Book" type="text" name="weight">
        </td>
        <td class="td">Please provide weight in kg format.</td>
      </tr>
      <tr id="Furniture">
        <td class="space">Height</td>
        <td class="td1">
          <input for="Furniture" type="text" name="height">
        </td>

        <td class="space">Width</td>
        <td class="td1">
          <input for="Furniture" type="text" name="width">
        </td>

        <td class="space">Length</td>
        <td class="td1">
          <input for="Furniture" type="text" name="length">
        </td>
        <td class="td">Please provide dimensions in HxWxL format.</td>
      </tr>
    </table>
    <!-- End of dropdown table -->

    <script type="text/javascript">
    // This JS function below will display a forms with special atributes for each product type based on user selection
      window.onload = function () {
        document.getElementById("DVD").style.display = 'none';
        document.getElementById("Book").style.display = 'none';
        document.getElementById("Furniture").style.display = 'none';
      }

      function onTypeSelected() {
        var type = document.getElementById("types").value;
        if(type == "DVD") {
          document.getElementById("DVD").style.display = '';
          document.getElementById("Book").style.display = 'none';
          document.getElementById("Furniture").style.display = 'none';
        }
        else if(type == "Book"){
          document.getElementById("DVD").style.display = 'none';
          document.getElementById("Book").style.display = '';
          document.getElementById("Furniture").style.display = 'none';
        }
        else{
          document.getElementById("DVD").style.display = 'none';
          document.getElementById("Book").style.display = 'none';
          document.getElementById("Furniture").style.display = '';
        }
      }
    </script>
  </body>
</html>
