/* Creating table manually */

CREATE TABLE tbl_product (
  id int(11) AUTO_INCREMENT PRIMARY KEY not null,
  type varchar (25) null,
  sku varchar (255) null,
  name varchar (255) null,
  price double (10,2) null,
  weight varchar (25) null,
  size varchar (25) null,
  height varchar (255) null,
  width varchar (255) null,
  length varchar (255) null
);

INSERT INTO tbl_product (type, sku, name, price, weight, size, height, width, length)
  VALUES ('$type','$sku','$name','$price','$weight', '$size', '$height', '$width', '$length');
